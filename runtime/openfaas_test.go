package runtime

import (
	"context"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlabktl/app/fs"
)

func TestOpenfaasRuntime_Build(t *testing.T) {
	t.Run("when runtime name is valid", func(t *testing.T) {
		details := Details{
			FunctionRuntime: "openfaas/classic/ruby",
			CodeDirectory:   "my/function/",
		}

		client := new(MockClient)
		defer client.AssertExpectations(t)

		file1 := new(MockFileInfo)
		defer file1.AssertExpectations(t)
		file1.On("Name").Return("Dockerfile")

		file2 := new(MockFileInfo)
		defer file2.AssertExpectations(t)
		file2.On("Name").Return("index.rb")

		file3 := new(MockFileInfo)
		defer file3.AssertExpectations(t)
		file3.On("Name").Return("function")

		client.On("ReadFile", "template/ruby/Dockerfile").
			Return("FROM openfaas/classic-watchdog:0.18.0", nil).Once()

		client.On("ReadFile", "template/ruby/index.rb").
			Return("require_relative 'function/handler'", nil).Once()

		client.On("ListFiles", "template/ruby").Return([]os.FileInfo{file1, file2, file3}, nil).Once()

		newRepositoryClient = func(l Location) (Client, error) { return client, nil }

		fs.WithTestFs(func() {
			runtime := OpenfaasRuntime{Details: details}
			err := runtime.Build(context.Background())
			require.NoError(t, err)

			dockerfile, err := fs.ReadFile("my/function/Dockerfile")
			require.NoError(t, err)

			index, err := fs.ReadFile("my/function/index.rb")
			require.NoError(t, err)

			assert.Equal(t, "FROM openfaas/classic-watchdog:0.18.0", string(dockerfile))
			assert.Equal(t, "require_relative 'function/handler'", string(index))
		})
	})

	t.Run("when runtime name is blank", func(t *testing.T) {
		runtime := OpenfaasRuntime{Details{FunctionRuntime: "openfaas/classic/"}}
		err := runtime.Build(context.Background())

		assert.Equal(t, "runtime name \"openfaas/classic/\" is incomplete", err.Error())
	})
}

func TestOpenfaasRuntime_BuildDryRun(t *testing.T) {
	t.Run("when runtime name is valid", func(t *testing.T) {
		runtime := OpenfaasRuntime{Details{FunctionRuntime: "openfaas/classic/ruby"}}

		summary, err := runtime.BuildDryRun()
		require.NoError(t, err)

		assert.Equal(t, "using OpenFaaS ruby runtime (dry-run)", summary)
	})

	t.Run("when runtime name is blank", func(t *testing.T) {
		runtime := OpenfaasRuntime{Details{FunctionRuntime: "openfaas/classic/"}}

		summary, err := runtime.BuildDryRun()

		assert.Equal(t, "runtime name \"openfaas/classic/\" is incomplete", summary)
		assert.Equal(t, "runtime name \"openfaas/classic/\" is incomplete", err.Error())
	})
}
