package runtime

import (
	"context"
	"fmt"
	"path"

	"github.com/gosimple/slug"

	"gitlab.com/gitlab-org/gitlabktl/logger"
)

const template = "Dockerfile.template"

type CustomRuntime struct {
	Details
}

// Build runtime files and persist them in CodeDirectory
func (runtime CustomRuntime) Build(ctx context.Context) error {
	logger.WithField("runtime", runtime.DockerfilePath()).
		Info("preparing a custom runtime")

	repository := NewRepository(runtime.FunctionRuntime)
	dockerfile, err := repository.ReadFile(template)

	if err != nil {
		logger.WithField("runtime", runtime.FunctionRuntime).
			WithError(err).Warn("could not read Dockerfile template")

		return fmt.Errorf("could not read Dockerfile template: %w", err)
	}

	template := Template{
		Filename:   runtime.DockerfilePath(),
		Attributes: runtime.Details,
		Contents:   dockerfile,
	}

	err = template.Write()
	if err != nil {
		logger.WithField("template", runtime.DockerfilePath()).
			WithError(err).Warn("could not write a runtime template")

		return fmt.Errorf("could not write a runtime template: %w", err)
	}

	err = repository.WriteFiles(runtime.CodeDirectory)
	if err != nil {
		logger.WithField("runtime", runtime.FunctionRuntime).
			WithError(err).Warn("could not write runtime context files")

		return fmt.Errorf("could not write runtime context files: %w", err)
	}

	return nil
}

func (runtime CustomRuntime) BuildDryRun() (string, error) {
	logger.WithField("runtime", runtime.DockerfilePath()).
		Info("preparing a custom runtime (dry-run)")

	return "preparing a custom runtime", nil
}

func (runtime CustomRuntime) Slug() string {
	return slug.Make(runtime.FunctionName)
}

func (runtime CustomRuntime) DockerfilePath() string {
	filename := "Dockerfile." + runtime.Slug()

	return path.Join(runtime.CodeDirectory, filename)
}
