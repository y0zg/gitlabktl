package knative

import (
	"context"
	"errors"
	"path"
	"strings"

	"gitlab.com/gitlab-org/gitlabktl/logger"
	"gitlab.com/gitlab-org/gitlabktl/serverless"
)

// Functions represent a set of Knative services and a root service used to
// deploy functions to a cluster
type Functions struct {
	namespace string
	registry  Registry
	cluster   Cluster
	functions []serverless.Function
}

func (fn *Functions) DeployDryRun() (string, error) {
	logger.Info("running functions dry-run deployment")

	return fn.deployServices(func() {
		fn.cluster.SetDryRun()
	})
}

func (fn *Functions) Deploy(ctx context.Context) (string, error) {
	logger.Info("running functions deployment")

	return fn.deployServices(func() {
		fn.cluster.SetNoDryRun()

		if fn.registry.HasCredentials() {
			fn.cluster.DeployRegistryCredentials()
		}
	})
}

func (fn *Functions) deployServices(before func()) (string, error) {
	services := fn.ToServices()

	if len(services) == 0 {
		return "no services", errors.New("no services to deploy")
	}

	before()

	return fn.cluster.DeployServices(services)
}

func (fn *Functions) ToServices() (services []Service) {
	for _, function := range fn.functions {
		service := Service{
			Namespace:   fn.namespace,
			Name:        function.Service,
			Image:       function.Image,
			Secrets:     function.Secrets,
			Labels:      append(function.Labels, "service:"+function.Service),
			Envs:        function.Envs,
			Annotations: function.Annotations,
		}

		if len(function.Image) == 0 {
			service.Image = path.Join(fn.registry.Repository, function.Name)
		}

		if len(function.Description) != 0 {
			if description, ok := service.Annotations["Description"]; ok {
				service.Annotations["Description"] = strings.Join([]string{description, function.Description}, "\n")
			} else {
				service.Annotations["Description"] = function.Description
			}
		}

		services = append(services, service)
	}

	return services
}
