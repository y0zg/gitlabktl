package knative

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/logger"
	"gitlab.com/gitlab-org/gitlabktl/serverless"
)

func TestFunctionsDeploy(t *testing.T) {
	WithMockCluster(func(cluster *MockCluster) {
		t.Run("when there are functions to deploy", func(t *testing.T) {
			cluster.On("SetNoDryRun").Once()
			cluster.On("DeployRegistryCredentials").Return(nil).Once()
			cluster.On("DeployServices", mock.Anything).Return("ok", nil).Once()
			defer cluster.AssertExpectations(t)

			functions := &Functions{
				functions: []serverless.Function{serverless.Function{}},
				registry:  Registry{Username: "user", Password: "pass"},
				namespace: "my-namespace",
				cluster:   cluster,
			}

			summary, err := functions.Deploy(context.Background())
			require.NoError(t, err)

			assert.Equal(t, "ok", summary)
		})

		t.Run("when there are no functions to deploy", func(t *testing.T) {
			functions := &Functions{
				functions: []serverless.Function{},
				registry:  Registry{},
				namespace: "my-namespace",
				cluster:   cluster,
			}

			_, err := functions.Deploy(context.Background())

			assert.EqualError(t, err, "no services to deploy")
		})
	})
}

func TestFunctionsDeployDryRun(t *testing.T) {
	WithMockCluster(func(cluster *MockCluster) {
		cluster.On("SetDryRun").Once()
		cluster.On("DeployServices", mock.Anything).Return("ok", nil).Once()
		defer cluster.AssertExpectations(t)

		functions := &Functions{
			functions: []serverless.Function{serverless.Function{}},
			registry:  Registry{Username: "user", Password: "pass"},
			namespace: "my-namespace",
			cluster:   cluster,
		}

		log := logger.WithTestLogger(func(logger *logger.Logger) {
			summary, err := functions.DeployDryRun()
			require.NoError(t, err)

			assert.Equal(t, "ok", summary)
		})
		assert.Contains(t, log.String(), "running functions dry-run deployment")
	})
}

func TestFunctionsToServices(t *testing.T) {
	WithMockCluster(func(cluster *MockCluster) {
		t.Run("when a function has an image defined", func(t *testing.T) {
			function := serverless.Function{
				Service:     "my-service",
				Description: "my-func-desc",
				Image:       "my.registry/my-service",
				Secrets:     []string{"my-secret"},
				Labels:      []string{"my-label"},
				Envs:        map[string]string{"my-env": "my-value"},
				Annotations: map[string]string{"my-annotation": "my-value"},
			}

			functions := &Functions{
				functions: []serverless.Function{function},
				registry:  Registry{Username: "user", Password: "pass"},
				namespace: "my-namespace",
				cluster:   cluster,
			}

			service := functions.ToServices()[0]

			assert.Equal(t, "my-namespace", service.Namespace)
			assert.Equal(t, function.Service, service.Name)
			assert.Equal(t, function.Image, service.Image)
			assert.Equal(t, function.Secrets, service.Secrets)
			assert.Equal(t, function.Envs, service.Envs)
			assert.Equal(t, "my-func-desc", service.Annotations["Description"])
			assert.Contains(t, service.Labels, "service:my-service")
		})

		t.Run("when a function does not have an image defined", func(t *testing.T) {
			function := serverless.Function{
				Service:     "my-service",
				Description: "my-func-desc",
				Envs:        map[string]string{"my-env": "my-value"},
				Annotations: map[string]string{"Description": "existing-value"},
			}

			functions := &Functions{
				functions: []serverless.Function{function},
				registry:  Registry{Repository: "my.default.registry/my-service"},
				namespace: "my-namespace",
				cluster:   cluster,
			}

			service := functions.ToServices()[0]

			assert.Equal(t, "my.default.registry/my-service", service.Image)
			assert.Equal(t, "existing-value\nmy-func-desc", service.Annotations["Description"])
		})
	})
}
