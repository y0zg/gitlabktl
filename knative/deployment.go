package knative

import (
	"fmt"

	"gitlab.com/gitlab-org/gitlabktl/serverless"
)

type Deployer serverless.Deployer

// We currently have both DeployFunctions() and DeployApplications() because
// `tm` tool deploys them differently.

func NewFunctionsDeployer(manifest serverless.Manifest, config Config) (Deployer, error) {
	cluster, err := NewCluster(config)
	if err != nil {
		return nil, fmt.Errorf("could not define a deployment cluster: %w", err)
	}

	functions := manifest.ToFunctions()

	return &Functions{
		functions: functions,
		cluster:   cluster,
		namespace: config.Namespace(),
		registry:  config.Registry()}, nil
}

func NewApplicationDeployer(service Service, config Config) (Deployer, error) {
	cluster, err := NewCluster(config)
	if err != nil {
		return nil, fmt.Errorf("could not define a deployment cluster: %w", err)
	}

	return &Application{
		service:  service,
		cluster:  cluster,
		registry: config.Registry()}, nil
}
