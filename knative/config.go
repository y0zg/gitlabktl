package knative

// Config is an interface that provides a cluster access details
type Config interface {
	Namespace() string  // Kubernetes namespace we want to interact with
	KubeConfig() string // Path to a cluster client config `.kubeconfig`
	Registry() Registry // Main registry we want to pull services from
}
